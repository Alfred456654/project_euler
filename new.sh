#!/bin/bash
git add .
git commit -m "bla"
git push origin master
# Delete when I reach p66
LAST=$(ls --color=none -d -1 p* | grep -v 'p67' | sed 's/[^0-9]\+//g' | sort -n | tail -n 1)
LAST=$((LAST+1))
A="p${LAST}"
mkdir "$A"
echo -e "#!/usr/bin/python3\nif __name__ == '__main__':\n    " > "${A}/${A}.py"
chmod +x "${A}/${A}.py"
