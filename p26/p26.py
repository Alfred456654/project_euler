#!/usr/bin/python3

def rec(b):
    a = 1
    seen = []
    cdt = True
    while cdt:
        c = int(a / b)
        a = a - b * c
        if (c, a) in seen:
            for j in range(seen.index((c, a))):
                seen.remove(seen[0])
            cdt = False
        else:
            seen.append((c, a))
            a = 10 * a
    if (0, 0) in seen:
        seen.remove((0, 0))
    return len(seen)

if __name__ == '__main__':
    m = 0
    m_i = 0
    for i in range(1, 1000):
        l = rec(i)
        if l > m:
            m = l
            m_i = i
    print("%d" % m_i)
