#!/usr/bin/python3

from math import sqrt

def is_p(n):
    if n < 0:
        n = -n
    if n == 1:
        return False
    if n % 2 == 0 and n > 2:
        return False
    for i in range(3, int(sqrt(n) + 1), 2):
        if n % i == 0:
            return False
    return True

def f(a, b, n):
    return n * n + a * n + b

if __name__ == '__main__':
    m_v = 0
    m_a = 0
    m_b = 0
    for a in range(1000):
        for b in range(1001):
            n = 0
            while is_p(f(a, b, n)):
                n += 1
            if m_v < n:
                m_v = n
                m_a = a
                m_b = b
            n = 0
            while is_p(f(-a, b, n)):
                n += 1
            if m_v < n:
                m_v = n
                m_a = -a
                m_b = b
            n = 0
            while is_p(f(a, -b, n)):
                n += 1
            if m_v < n:
                m_v = n
                m_a = a
                m_b = -b
            n = 0
            while is_p(f(-a, -b, n)):
                n += 1
            if m_v < n:
                m_v = n
                m_a = -a
                m_b = -b
    print(m_a * m_b)
