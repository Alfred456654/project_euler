#!/usr/bin/python3
if __name__ == '__main__':
    s1 = sum(range(101))
    s2 = sum([x*x for x in range(101)])
    print(s1*s1-s2)
