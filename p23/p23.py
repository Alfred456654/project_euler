#!/usr/bin/python3

from math import sqrt

def d(n):
    if n == 1:
        return 0
    ans = 0
    for i in range(1, 1 + int(sqrt(n))):
        if n % i == 0:
            ans += i
            if i > 1 and i * i != n:
                ans += int(n / i)
    return ans

def crit(ab, n):
    for i in ab:
        if n - i in ab:
            return True
    return False

if __name__ == '__main__':
    ab = {}
    ans = 0
    for i in range(1, 28124):
        if i < d(i):
            ab.update({i: 0})
        if not crit(ab, i):
            ans += i
    print(ans)
