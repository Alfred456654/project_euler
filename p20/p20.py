#!/usr/bin/python3

from math import factorial

if __name__ == '__main__':
    print(sum([int(x) for x in str(factorial(100))]))
