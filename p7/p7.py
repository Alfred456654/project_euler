#!/usr/bin/python3

from math import sqrt

def prim(n):
    for i in range(2, int(sqrt(n)) + 1):
        if n % i == 0:
            return False
    return True

if __name__ == '__main__':
    c = 1
    p = 2
    while c < 10001:
        p += 1
        if prim(p):
            c += 1
    print(p)
