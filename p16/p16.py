#!/usr/bin/python3

from math import pow

if __name__ == '__main__':
    n = 0
    for s in str(int(pow(2, 1000))):
        n += int(s)
    print(n)
