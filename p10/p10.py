#!/usr/bin/python3

from math import sqrt

def prim(n):
    for i in range(2, int(sqrt(n)) + 1):
        if n % i == 0:
            return False
    return True

if __name__ == '__main__':
    s = 0
    n = 1
    while n < 2000000:
        n += 1
        if prim(n):
            s += n
    print(s)
