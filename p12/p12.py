#!/usr/bin/python3

m_d = 0
n = 0
inc = 0

def nb_div(n):
    global m_d
    cpt = 1
    for i in range(1, 1 + int(n / 2)):
        if n % i == 0:
            cpt += 1
    if cpt > m_d:
        print("%d has %d divisors" % (n, cpt))
        m_d = cpt
    return cpt

while nb_div(n) < 500:
    inc += 1
    n += inc
print(n)
