#include <stdio.h>
#include <stdlib.h>

int m_d;

int nb_div(int n) {
    int cpt = 1;
    for(int i = 1; i <= 1 + n/2; i++) {
        if(n % i == 0) {
            cpt++;
        }
    }
    if(cpt > m_d) {
        printf("%d has %d divisors\n", n, cpt);
        m_d = cpt;
    }
    return cpt;
}

int main(int argc, char *argv[]) {
    int n = 0, inc = 0;
    m_d = 0;
    while(nb_div(n) < 500) {
        inc++;
        n += inc;
    }
    printf("answer: %d\n", n);
    return 0;
}
