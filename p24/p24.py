#!/usr/bin/python3

from math import factorial as f

if __name__ == '__main__':
    t = 999999
    digits = [x for x in range(10)]
    for i in range(len(digits) - 1, 0, -1):
        q = int(t / f(i))
        t = t % f(i)
        print(digits[q], sep='', end='', flush=True)
        digits.remove(digits[q])
    print("%d" % digits[0])
