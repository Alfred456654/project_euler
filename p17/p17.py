#!/usr/bin/python3

def to_str(nb):
    global UNITS, TEENS, DOZENS, HUNDRED_AND, HUNDRED
    if type(nb) is not int:
        raise Exception('nb must be an integer, got %s instead' % type(nb))
    if nb < 1 or nb > 1000:
        raise Exception('nb must be between 1 and 1000, got %d instead' % nb)
    if nb == 1000:
        return 'onethousand'
    s = str(nb)
    l = len(s)
    dozens = 0
    hundreds = 0
    word = ''
    if l > 1:
        dozens = int(s[-2])
    units = int(s[-1])
    if l > 2:
        hundreds = int(s[-3])
        if dozens + units > 0:
            word = '%s%s' % (UNITS[hundreds], HUNDRED_AND)
        else:
            return '%s%s' % (UNITS[hundreds], HUNDRED)
    if dozens == 1:
        word += TEENS[units]
        return word
    elif dozens > 1:
        word += DOZENS[dozens]
        word += UNITS[units]
        return word
    else:
        return word + UNITS[units]

if __name__ == '__main__':
    UNITS = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
    TEENS = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
    DOZENS = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']
    HUNDRED = 'hundred'
    HUNDRED_AND = 'hundredand'

    c = 0
    for i in range(1, 1001):
        s = to_str(i)
        print("%d [%d] : %s" % (i, len(s), s))
        c += len(s)
    print(c)
