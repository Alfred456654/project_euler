#!/usr/bin/python3

from math import factorial as fact

if __name__ == '__main__':
    print(int(fact(40)/(fact(20) * fact(20))))
