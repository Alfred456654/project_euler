#!/usr/bin/python3

if __name__ == '__main__':
    p = []
    with open('input.txt', 'r') as txt:
        for l in txt.readlines():
            p.append([x for x in map(int, l.split(' '))])
    sums = [p[0]]
    for i in range(1, len(p)):
        prow = sums[i-1]
        row = p[i]
        srow = [row[0] + prow[0]]
        for j in range(1, len(row) - 1):
            srow.append(max(row[j] + prow[j - 1], row[j] + prow[j]))
        srow.append(row[len(row) - 1] + prow[len(row) - 2])
        sums.append(srow)
    print(max(sums[len(sums) - 1]))

