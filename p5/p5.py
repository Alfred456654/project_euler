#!/usr/bin/python3

def ppcm(nb):
    maxv = max(nb)
    n = maxv
    while any(n % x for x in nb):
        n += maxv
    return n

if __name__ == '__main__':
    nb = [ x for x in range(2, 21) ]
    print(ppcm(nb))
