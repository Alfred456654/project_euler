#!/usr/bin/python3

if __name__ == '__main__':
    a = 1
    b = 1
    ans = 0
    while b < 4000000:
        if b % 2 == 0:
            ans += b
        c = a
        a = b
        b = a + c
    print(ans)
