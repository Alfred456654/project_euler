#!/usr/bin/python3
if __name__ == '__main__':
    ans = 0
    for i in range(2, 200000):
        t = 0
        for j in str(i):
            t += int(j) ** 5
        if i == t:
            ans += t
    print(ans)
