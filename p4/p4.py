#!/usr/bin/python3

def rev(n):
    return int(''.join([n[i] for i in range(len(n) - 1, -1, -1)]))

if __name__ == '__main__':
    pals = []
    for a in range(999, 100, -1):
        for b in range(a - 1, 99, -1):
            if rev(str(a * b)) == a * b:
                pals.append(a * b)
    print(max(pals))
