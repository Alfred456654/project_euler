#!/usr/bin/python3
if __name__ == '__main__':
    a = 1
    b = 1
    ans = 2
    while b < 10**999:
        ans += 1
        c = a
        a = b
        b = a + c
    print(ans)
