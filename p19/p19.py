#!/usr/bin/python3

from datetime import date

if __name__ == '__main__':
    d1 = date(1901, 1, 1).toordinal()
    d2 = date(2000, 12, 31).toordinal()
    print(len([ x for x in map(date.fromordinal, range(d1, d2)) if (date.weekday(x) == 6 and x.day == 1) ]))
