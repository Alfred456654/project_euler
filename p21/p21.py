#!/usr/bin/python3

from math import sqrt

def d(n):
    if n == 1:
        return 0
    ans = 0
    for i in range(1, 1 + int(sqrt(n))):
        if n % i == 0:
            ans += i
            if i > 1:
                ans += int(n / i)
    return ans

if __name__ == '__main__':
    nb = {}
    ans = 0
    for i in range(1, 10000):
        i_d = d(i)
        nb.update({i: i_d})
        if i_d in nb and i_d < i and nb[i_d] == i:
            ans += i + i_d
    print(ans)
