#!/usr/bin/python3
if __name__ == '__main__':
    ans = 0
    with open('p022_names.txt', 'r') as txt:
        names = sorted(txt.readlines()[0].replace('"', '').split(','))
        for n in range(len(names)):
            ans += (n + 1) * sum([ord(x) - 64 for x in names[n]])
    print(ans)

