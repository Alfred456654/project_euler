#!/usr/bin/python3

def chain(n, d):
    if n == 1:
        return d + 1
    if n % 2 == 0:
        return chain(n / 2, d + 1)
    else:
        return chain(3 * n + 1, d + 1)

if __name__ == '__main__':
    m = 0
    m_i = 0
    for i in range(1, 1000000):
        l = chain(i, 0)
        if l > m:
            m = l
            m_i = i
            print("%d produces a chain of %d" % (m_i, m))
